﻿using System;
using System.Linq;
using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace SirinInteractive.Utilities 
{
    public class CacheEntriesCreator 
    {
#if UNITY_EDITOR
        

        [MenuItem("Tools/Generate cached entries", priority = 1)]
        private static void GenerateEntries()
        {
            var types = Assembly.GetCallingAssembly()
                .GetTypes()
                .Where(t => 
                t.BaseType != null && 
                t.BaseType.IsGenericType &&
                t.IsAbstract == false &&
                (t.BaseType.GetGenericTypeDefinition() == typeof(CachedEntry<>) ||
                    t.BaseType.BaseType != null &&
                    t.BaseType.BaseType.IsGenericType &&
                    t.BaseType.BaseType.GetGenericTypeDefinition() == typeof(CachedEntry<>))
                );

            foreach (var type in types)
            {
                Debug.Log($"{type}");

                var instanceProperty = type.GetProperty("Instance", 
                    BindingFlags.Public | BindingFlags.Static | BindingFlags.FlattenHierarchy);

                instanceProperty.GetValue(null);

            }
        }
#endif
    }
}