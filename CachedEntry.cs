﻿using ModestTree;
using System;
using System.Linq;
using System.Reflection;
using UnityEditor;
using UnityEngine;
namespace SirinInteractive.Utilities 
{
	public class CachedEntry<T> : ScriptableObject where T : ScriptableObject
	{
		public static T Instance
		{
			get
			{
				var path = string.Empty;
				CachedEntryPathAttribute pathAttribute = null;

                try
				{
                    pathAttribute = typeof(T).GetAttribute<CachedEntryPathAttribute>();
                }
				catch (Exception e) { }

				if (pathAttribute != null)
					path += $"{pathAttribute.path}";

                var result = Resources.Load<T>($"{path}/{typeof(T)}");

				if (result == null)
				{
#if UNITY_EDITOR
					result = ScriptableObject.CreateInstance<T>();
					var folderPath = $"Resources/{path}";

                    if (AssetDatabase.IsValidFolder("Assets/" + folderPath) == false)
					{
                        Debug.Log(Application.dataPath + "/" + folderPath);
                        var absPath = Application.dataPath + "/" + folderPath;
						
                        System.IO.Directory.CreateDirectory(absPath);
                        EditorUtility.RevealInFinder(absPath);

                        AssetDatabase.Refresh();
                    }

					Debug.Log($"Could not load - {path}/{typeof(T)}");

                    AssetDatabase.CreateAsset(result, $"Assets/{folderPath}/{typeof(T)}.asset");
                    AssetDatabase.SaveAssets();
#endif
				}

				return result;
			}
		}
	}

	public class CachedEntryPathAttribute : Attribute
	{
		public string path;
		public CachedEntryPathAttribute(string path)
		{
			this.path = path;
		}
	}
}