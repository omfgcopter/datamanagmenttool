﻿using Sirenix.Utilities.Editor;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using static UnityEngine.Rendering.DebugUI;

namespace SirinInteractive.Utilities
{
    [System.Serializable]
    public class SirinSerializedDictionary<Key, Value> : ISerializationCallbackReceiver, IDictionary<Key, Value>
    {
        private Dictionary<Key, Value> dictionary = new Dictionary<Key, Value>();

        private List<Key> keys = new List<Key>();

        [SerializeField] private List<Entry> entries = new List<Entry>();

        public Value this[Key key]
        {
            get => dictionary[key];
            set => dictionary[key] = value;
        }

        public int Count => dictionary.Count;

        public ICollection<Key> Keys => dictionary.Keys;

        public ICollection<Value> Values => dictionary.Values;

        public bool IsReadOnly => false;

        public void Add(Key key, Value value)
        {
            if (dictionary.TryAdd(key, value))
            {
                entries.Add(new Entry() { key = key, value = value });
            }
            else
            {
                throw new System.Exception($"Alredy contains key - {key}");
            }
        }

        public void Add(KeyValuePair<Key, Value> item)
        {
            if (dictionary.TryAdd(item.Key, item.Value))
            {
                entries.Add(new Entry() { key = item.Key, value = item.Value });
            }
            else
            {
                throw new System.Exception($"Alredy contains key - {item.Key}");
            }
        }

        public void Clear()
        {
            dictionary.Clear();
            entries.Clear();
            keys.Clear();
        }

        public bool Contains(KeyValuePair<Key, Value> item)
        {
            return dictionary.Contains(item);
        }

        public bool ContainsKey(Key key)
        {
            return dictionary.ContainsKey(key);
        }

        public void CopyTo(KeyValuePair<Key, Value>[] array, int arrayIndex)
        {
            throw new System.NotImplementedException();
        }

        public IEnumerator<KeyValuePair<Key, Value>> GetEnumerator()
        {
            return dictionary.GetEnumerator();
        }

        public void OnAfterDeserialize()
        {
            dictionary.Clear();

            for (int i = 0; i < entries.Count; i++)
            {
                dictionary.Add(entries[i].key, entries[i].value);
            }
        }

        public void OnBeforeSerialize()
        {
            if (entries.Count == 0)
            {
                return;
            }

            keys.Clear();

            for (int i = 0; i < keys.Count; i++)
            {
                keys.Add(entries[i].key);
            }

            var result = keys.GroupBy(x => x)
                        .Where(g => g.Count() > 1)
                        .Select(x => new { Element = x.Key, Count = x.Count() })
                        .ToList();

            if (result.Count > 0)
            {
                var duplicates = string.Join(", ", result);
                Debug.LogError($"Warning 'SerializableDictionary' keys has duplicates {duplicates}");
            }
        }

        public bool Remove(Key key)
        {
            var entry = entries.SingleOrDefault(x => x.key.Equals(key));

            if (entry == null)
            {
                dictionary.Remove(key);
                keys.Remove(key);
                return false;
            }

            entries.Remove(entry);

            return dictionary.Remove(key);
        }

        public bool Remove(KeyValuePair<Key, Value> item)
        {
            return Remove(item.Key);
        }

        public bool TryGetValue(Key key, out Value value)
        {
            return dictionary.TryGetValue(key, out value);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return dictionary.GetEnumerator();
        }

        [System.Serializable]
        public class Entry
        {
            public Key key;
            public Value value;
        }

        public override string ToString()
        {
            return string.Join("\n", entries.Select(x => $"{x.key} - {x.value}"));
        }
    }
}
