using Sirenix.OdinInspector;
using Sirenix.OdinInspector.Editor;
using Sirenix.Utilities;
using Sirenix.Utilities.Editor;
using SirinInteractive.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEditor;
using UnityEngine;
using Zenject;

namespace SirinInteractive.Data

{
    [Serializable]
    public class DataConfig : ICloneable
    {
        public string Name;
        public string Path;
        [HideInInspector] public string Guid;

        public string FormattedName => 
            string.IsNullOrWhiteSpace(Name) ? "item" :
            string.IsNullOrWhiteSpace(Path) ? Name : $"{Path}/{Name}";

        public DataConfig(string guid)
        {
            this.Guid = guid;
        }

        public object Clone()
        {
            return MemberwiseClone();
        }
    }


    [Serializable]
    public abstract class SOConfigsCollection<Collection, Value> : CachedEntry<Collection>, IDataCollection<Value> 
        where Value : DataConfig
        where Collection : ScriptableObject
    {
        public DataConfigCollection<Value> dataConfigsCollection;
        [HideInInspector] public DataConfigCollection<Value> runtimeDataConfigsCollection;

        public Value GetItem(string guid)
        {
            return dataConfigsCollection.GetItem(guid);
        }

        public Value GetRuntimeItem(string guid)
        {
            return runtimeDataConfigsCollection.GetItem(guid);
        }

        public ICollection<DataConfig> AllConfigItems()
        {
            return dataConfigsCollection.GetAllItems();
        }

        [Inject]
        public virtual void Initialize()
        {
            runtimeDataConfigsCollection = new DataConfigCollection<Value>();

            foreach (var pair in dataConfigsCollection.data)
            {
                runtimeDataConfigsCollection.AddItem(pair.Value.Clone() as Value);
            }
        }
    }


    [System.Serializable]
    public class DataConfigCollection<T> where T : DataConfig
    {
        public SirinSerializedDictionary<string, T> data = new SirinSerializedDictionary<string, T>();


        [Button]
        public void AddItem()
        {
            T newObject = (T)Activator.CreateInstance(typeof(T), System.Guid.NewGuid().ToString());

            AddItem(newObject);
        }

        public void AddItem(T item)
        {
            data.Add(item.Guid, item);
        }

        public T GetItem(string id)
        {
            return data[id];
        }

        public ICollection<DataConfig> GetAllItems()
        {
            List<DataConfig> list = new List<DataConfig>();
            list.AddRange(data.Values);
            return list;
        }

        public override string ToString()
        {
            return data.ToString();
        }
    }

    public class DataConfigCollectionDrawer<Key, Value> : OdinValueDrawer<SirinSerializedDictionary<Key, Value>>
        where Value : DataConfig
    {
        private UnityEngine.Object selectedObject;

        private Dictionary<string, bool> foldout = new Dictionary<string, bool>();

        protected override void Initialize()
        {
            base.Initialize();

            selectedObject = Selection.activeObject;
            foldout.Clear();
            foreach (var child in this.Property.Children)
            {
                foldout.Add(child.Path, false);
            }
        }

        protected override void DrawPropertyLayout(GUIContent label)
        {
            EditorGUILayout.Space(10);

            foreach (var child in this.Property.Children)
            {
                var key = child.Children.Single(x => x.ValueEntry.TypeOfValue == typeof(string));

                var value = child.Children.Single(x => x.ValueEntry.TypeOfValue == typeof(Value));

                var strinKey = (key.ValueEntry.WeakSmartValue.ToString());
                var val = (value.ValueEntry.WeakSmartValue as Value);

                if (foldout.TryGetValue(child.Path, out bool foldoutValue))
                {
                        
                }
                else
                {
                    foldout.Add(child.Path, true);
                }

                var name = val.FormattedName;

                if (strinKey.Equals(val.Guid) == false)
                {
                    name += $" (!)";
                }

                foldout[child.Path] = SirenixEditorGUI.Foldout(foldout[child.Path], name );
                if (foldout[child.Path])
                {
                    EditorGUILayout.BeginHorizontal();

                    EditorGUILayout.LabelField(val.Guid);

                    Rect rect = EditorGUILayout.GetControlRect();
                    rect.width = rect.width * 0.5f;
                    if (GUI.Button(rect, "Copy GUID"))
                    {
                        EditorGUIUtility.systemCopyBuffer = key.ValueEntry.WeakSmartValue.ToString();
                    }

                    rect.x += rect.width;
                    if (GUI.Button(rect, "Delete"))
                    {
                        this.ValueEntry.SmartValue.Remove((Key)key.ValueEntry.WeakSmartValue);
                        EditorUtility.SetDirty(selectedObject);
                        this.ValueEntry.ApplyChanges();
                        AssetDatabase.Refresh();
                    }

                    EditorGUILayout.EndHorizontal();
                    EditorGUILayout.Space(3);
                    foreach (var child3 in value.Children)
                    {
                        if (child3?.ValueEntry?.Property?.GetAttribute<HideInInspector>() != null)
                            continue;

                        try
                        {
                            child3.Draw();
                        }
                        catch (Exception ex)
                        {
                            Debug.LogError($"Could not draw - {child3.NiceName}");
                        }
                        
                    }

                    EditorGUILayout.Space(10);
                }
            }
        }
    }

    public interface IDataCollection<T> where T : DataConfig
    {
        T GetItem(string guid);
        ICollection<DataConfig> AllConfigItems();
    }

    public class DataConfigFieldAttribute : Attribute
    {
        public Type dataConfigTargetType;

        public DataConfigFieldAttribute(Type dataConfigTargetType)
        {
            this.dataConfigTargetType = dataConfigTargetType;
        }
    }


    internal sealed class DataConfigAttributeProcessor<T> : OdinAttributeProcessor<T> where T : class
    {
        public override bool CanProcessSelfAttributes(InspectorProperty property)
        {
            var attr = property.Attributes.GetAttribute<DataConfigFieldAttribute>();

            if (attr == null) return false;

            return true;
        }

        public override void ProcessSelfAttributes(InspectorProperty property, List<Attribute> attributes)
        {
            var lds = property.Attributes.GetAttribute<ValueDropdownAttribute>();

            if (lds == null)
            {
                var valuesGetter = $"@DataConfigAttributeUtilities.GetCommandsList($property)";
                lds = new ValueDropdownAttribute(valuesGetter);
                attributes.Add(lds);
            }
        }
    }

    public class DataConfigAttributeUtilities
    {
        //This function is referenced by string, even if you see 0 references - dont delete
        private static ValueDropdownList<string> GetCommandsList(InspectorProperty property)
        {
            var attr = property.Attributes.GetAttribute<DataConfigFieldAttribute>();
            var type = attr.dataConfigTargetType;

            var dataConfigAttr = type.GetCustomAttribute<DataConfigAttribute>();

            var getInstanceMethod = dataConfigAttr.dataConfigCollectionType.GetProperty("Instance", BindingFlags.Public | BindingFlags.Static | BindingFlags.FlattenHierarchy);

            var instanceOfCollection = getInstanceMethod.GetValue(null);

            var getCollectionsMethod = dataConfigAttr.dataConfigCollectionType.GetMethod("AllConfigItems");

            var collection = (getCollectionsMethod.Invoke(instanceOfCollection, null)) as List<DataConfig>;

            var result = new ValueDropdownList<string>();

            collection.ForEach(x => result.Add(x.FormattedName, x.Guid));

            return result;
        }
    }

    public class DataConfigAttribute : Attribute
    {
        public Type dataConfigCollectionType;

        public DataConfigAttribute(Type dataConfigCollectionType)
        {
            this.dataConfigCollectionType = dataConfigCollectionType;
        }
    }


}