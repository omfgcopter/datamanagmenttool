# Data Config

## Summary

A simple way to organize your data in a Unity project. This tool requiers Odin Inspector!

Any system you working on require a data, that can be easily edited, accessed and cross referenced. 

Idea behind this tool is rather simple, you can create a class, that will represent a data model, and a scriptable object will be created for you to access and edit it.

This collection will come with following benefits:
	-Instance property, that you can use to access it directly, or reference it through injection system.
	-DataConfigField attribute, that allow you to access different DataConfig collection in a nice drop down list in inspector.
	-Items can be grouped in categories and searched thanks to Odin Inspector capabilities.
	

## Examples

We need to create 2 classes, and because we will store data in a scriptable object, we should name the file TestConfigsCollection, so Unity will not have any problem creating and accessing it. 

Colleciton must inherit generic SOConfigsCollection<CollecitonClass, DataModelClass>, in first generic parameter you put the same collection you just created, the second parameter is for your model class.


```
    public class TestConfigsCollection : SOConfigsCollection<TestConfigsCollection, TestConfig>			//This is a collecion class, that you use to access data.
    {
    }


    [System.Serializable]
    [DataConfig(typeof(TestConfigsCollection))]							//A reference to collection
    public class TestConfig : DataConfig								//This is a data model, add any fields and properties that you need
    {
        [DataConfigField(typeof(AnotherTestConfig))]					//This is a reference to another data type, thats what will give you a nice drop down in inspector
        public string dataId;

        public TestConfig(string guid) : base(guid) { }					//You need to create constructor, you can leave it empty
    }
```

We have referenced a AnotherTestConfig in our first data model, so lets create it as well.

```
	[CachedEntryPath("Folder/Path")]		//You can specify folder, where scriptable object will be created, relative to Resources folder.
    public class AnotherTestConfigsCollection : SOConfigsCollection<AnotherTestConfigsCollection, AnotherTestConfig>
    {
    }

    [System.Serializable]
    [DataConfig(typeof(AnotherTestConfigsCollection))]
    public class AnotherTestConfig : DataConfig
    {
        [DataConfigField(typeof(ShipDefinitionConfig))]
        public int number;

        public AnotherTestConfig(string guid) : base(guid) { }
    }
```

After that, in editor click on Tools>Generate cached entries, and 2scriptable objects should be created, one in Resources folder, and another in Resources/Folder/Path.

Edit your data through this objects.

![another test data inspector](img/anotherTestData.png)
![test data inspector](img/testData.png)

Then you can access you data in code like this:


```
	TestConfigsCollection.Instance.GetItem(guid);
```